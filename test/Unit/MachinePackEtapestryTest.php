<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackEtapestryTest extends TestCase
{
    /**
     * Test a single gift creation (no payment)
     */
    public function debugValidGiftCreation()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event   = $this->_createValidEvent('oneOffGift');
        $results = MachinePack::send(
            'crmsync.create.etapestry',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test a recurring gift creation (no payment)
     */
    public function debugValidRecurringGiftCreation()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event   = $this->_createValidEvent('recurringGift');
        $results = MachinePack::send(
            'crmsync.create.etapestry',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test a valid one-off credit card payment
     */
    public function debugValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event   = $this->_createValidEvent('oneOffGiftAndPayment');
        $results = MachinePack::send(
            'payment.create.etapestry',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test a valid Recurring credit card payment
     */
    //Recurring payments do not work with dummy card
    public function debugValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidEvent('recurringGiftAndPayment');
        $event['Product.productId'] = 'DONONLINE_RECURRING';
        // Payment frequency Weekly -> 52, Monthly -> 12, Semi-Annually ->2, Yearly -> 1
        $event['Product/Subscription.frequency'] = '12';

        $results = MachinePack::send(
            'subscription.create.etapestry',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * Test an invalid one-off credit card payment
     */
    /* public function debugInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '333322221111';

        $results = MachinePack::send(
            'payment.create.etapestry',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    public function testPipelineSuccess()
    {
        $this->assertInstanceOf(Success::class, new Success());
    }

    /**
     * Creates a valid payment event
     */
    private function _createValidEvent($action)
    {
        //NOTE ETAPESTRY HAS NO SANDBOX, AND REQUIRES A LIVE CARD.
        //THIS TEST CARD MAY ONLY WORK ON SOME INSTANCES.
        $event = array();
        $event['Intangible/receipt.no']         = uniqid();
        $event['Person.existingID']             = '13275.0.210326551';
        $event['Order.identifier']              = uniqid();
        $event['Order.paymentType']             = 'Once'; // oneTime or recurring
        $event['Order.description']             = 'Test transaction';
        $event['Person.givenName']              = 'John';
        $event['Person.familyName']             = 'Doe';
        $event['Person.telephone']              = '0404000000';
        $event['Person.email']                  = 'dev123@leafcutter.com.au';
        $event['PostalAddress.streetAddress']   = '123 Nowhere St';
        $event['PostalAddress.addressLocality'] = 'Crows Nest';
        $event['PostalAddress.addressRegion']   = 'NSW';
        $event['PostalAddress.postalCode']      = '2065';
        $event['PostalAddress.addressCountry']  = 'AU';
        $event['CreditCard/CardDetails.name']   = 'James Hornitzky';
        $event['CreditCard/CardDetails.number'] = '9999888877776666';
        $event['CreditCard/CardDetails.expiryMonth'] = '01';
        $event['CreditCard/CardDetails.expiryYear']  = '2025';
        $event['CreditCard/CardDetails.cvn']         = '111';
        $event['CreditCard/CardDetails.type']        = 'Visa';
        $event['Intangible/Gateway.fund']            = 'General';
        $event['Intangible/Gateway.campaign']        = 'Annual';
        $event['Intangible/Gateway.approach']        = 'Unsolicited';
        $event['MonetaryAmount.value']               = 1.55;
        $event['MonetaryAmount.currency']            = 'AUD';
        //Optional for subscription
        if ($action == 'recurringGiftAndPayment') {
            $event['Intangible/Timezone.local']                    = "Australia/Sydney";
            $event['Intangible/Timezone.etapestry']                = 'America/New_York';
            $event['Intangible/Subscription.firstInstallmentDate'] = date('d/m/Y');
        }

        //Optional for one-off gift
        if ($action == 'oneOffGift') {
            $event['Intangible/Etapestry.action'] = 'createContactAndGift';
            $event['Intangible/Event.payload']    = []; //CRMSYNC Event requires this
            $event['Intangible/Gateway.name']     = 'Test';
            $event['Intangible/receipt.no']       = 't_00001';
        }

        //Optional for recurring gift
        if ($action == 'recurringGift') {
            $event['Intangible/Etapestry.action']   = 'createContactAndRecurringGift';
            $event['Intangible/Event.payload']      = []; //CRMSYNC Event requires this
            $event['Intangible/Gateway.name']       = 'Recurring Test';
            $event['Intangible/receipt.no']         = 's_00001';
            $event['Intangible/Timezone.local']     = "Australia/Sydney";
            $event['Intangible/Timezone.etapestry'] = 'America/New_York';
            $event['Intangible/Subscription.firstInstallmentDate'] = date('d/m/Y');
        }

        //Use this if you want to update existing account data
        //$event['Intangible/Contact.ifExist'] = 'update';

        return $event;
    }
}
