<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Result\Success;

final class MachinePackCauseViewTest extends TestCase
{

    /**
     * Create a transaction
     */
    /* public function testTransactionCreationObject()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results     = MachinePack::send('payment.create.causeview', $event);
        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */
    public function test()
    {
        $firstResult = new Success(true);
        $this->assertInstanceOf(Success::class, $firstResult);
    }

    /**
     * @return array
     */
    private function _createValidPaymentEvent()
    {
        $event = array();

        $event = [
            "method" => "POST",
            "MonetaryAmount.currency" => "AUD",
            "MonetaryAmount.value" => 15,
            "Intangible/Event.payload" => [[
                "gateway_type" => "EziDebit",
                "action_page_id" => "a0J3c00000sErhREAS",
                "field" => [
                    [
                        "id" => "contactSFID",
                        "name" => "VALUE",
                        "value" => "",
                    ],
                    [
                        "id"=> "salutationCombo",
                        "name"=> "VALUE",
                        "value"=> "Mr.",
                    ],
                    [
                        "id"=> "donorName",
                        "name"=> "FIRSTNAME",
                        "value"=> "Leafcutter",
                    ],
                    [
                        "id"=> "donorName",
                        "name"=> "LASTNAME",
                        "value"=> "Test",
                    ],
                    [
                        "id"=> "donorName",
                        "name"=> "DONATIONTYPE",
                        "value"=> "IND",
                    ],
                    [
                        "id"=> "recurringCombo",
                        "name"=> "ISRECURRING",
                        "value"=> false,
                    ],
                    [
                        "id"=> "ticketSaleQuantity",
                        "name"=> "VALUE",
                        "value"=> "2",
                    ],
                    [
                        "id"=> "ticketSaleAmount",
                        "name"=> "VALUE",
                        "value"=> "6",
                    ],
                    [
                        "id"=> "ticketEventSFID",
                        "name"=> "VALUE",
                        "value"=> "7012a000000BmMi",
                    ],
                    [
                        "id"=> "ticketFundSFID",
                        "name"=> "VALUE",
                        "value"=> "a0M2a0000018xiA",
                    ],
                    [
                        "id"=> "donateAmount",
                        "name"=> "VALUE",
                        "value"=> "4",
                    ],
                    [
                        "id"=> "donateApealSFID",
                        "name"=> "VALUE",
                        "value"=> "7012a000000BmMd",
                    ],
                    [
                        "id"=> "donateFundSFID",
                        "name"=> "VALUE",
                        "value"=> "a0M2a0000018xi5",
                    ],
                    [
                        "id"=> "merchandiseSaleQuantity",
                        "name"=> "VALUE",
                        "value"=> "3",
                    ],
                    [
                        "id"=> "merchandiseSaleAmount",
                        "name"=> "VALUE",
                        "value"=> "5",
                    ],
                    [
                        "id"=> "merchandiseApealSFID",
                        "name"=> "VALUE",
                        "value"=> "7012a000000BmMi",
                    ],
                    [
                        "id"=> "merchandiseFundSFID",
                        "name"=> "VALUE",
                        "value"=> "a0M2a0000018xiA",
                    ],
                    [
                        "id"=> "totalSumAmount",
                        "name"=> "VALUE",
                        "value"=> "15",
                    ],
                    [
                        "id"=> "letterSFID",
                        "name"=> "VALUE",
                        "value"=> "a0W4100000EayOD",
                    ],
                    [
                        "id"=> "payerFirstname",
                        "name"=> "VALUE",
                        "value"=> "John",
                    ],
                    [
                        "id"=> "payerLastname",
                        "name"=> "VALUE",
                        "value"=> "Smith",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "PAYMENTTYPE",
                        "value"=> "Credit Card",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "TYPE",
                        "value"=> "MasterCard",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "NUMBER",
                        "value"=> "5123456789012346",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "EXPIRYYEAR",
                        "value"=> "2021",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "EXPIRYMONTH",
                        "value"=> "05",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "EXPIRATIONDATE",
                        "value"=> "2021-05-01",
                    ],
                    [
                        "id"=> "ccInfo",
                        "name"=> "CVV2",
                        "value"=> "123",
                    ],
                    [
                        "id"=> "autoEmailReceipt",
                        "name"=> "VALUE",
                        "value"=> false,
                    ],
                    [
                        "id"=> "softcreditCheckbox",
                        "name"=> "VALUE",
                        "value"=> false,
                    ]
                ],
            ]],
        ];
        return $event;
    }
}
