<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Crmsync;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackImisTest extends TestCase
{
    /**
     * Test a valid one-off credit card payment
     */
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidCRMEvent();

        $results = MachinePack::send(
            'crmsync.create.imis',
            $event
        );

        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Creates a valid payment event
     */
    private function _createValidCRMEvent()
    {
        $event = array(
            'Order.identifier' => '29673618',
            'Order.description' => 'marlin admin ezdebit Donation',
            'Order.orderDate' => '2021-08-30',
            'Order.paymentMethod' => 'Credit Card',
            'Order.orderStatus' => 'Ok',
            'MonetaryAmount.currency' => 'AUD',
            'MonetaryAmount.value' => '75.00',
            'Person.givenName' => 'Marlin',
            'Person.familyName' => 'Admin',
            'Person.email' => 'test@marlincommunications.com',
            'Person.telephone' => '0425132242',
            'PostalAddress.postalCode' => '2065',
            'CreditCard/CardDetails.token' => '2577889',
            'Product.productId' => '',
            'Intagible/Event.utm_source' => 'test',
            'Intagible/Event.utm_medium' => 'test',
            'Intagible/Event.utm_campaign' => 'test',
            'Intagible/Event.utm_content' => 'test',
            'Intagible/Event.utm_term' => '',
            'Intangible/Event.payload' => '',
            'Intangible/Api.apiUrl' => 'https://slswa.imiscloud.com',
            'Intangible/Api.username' => 'MARLIN',
            'Intangible/Api.password' => 'Surf_sAFE',
        );
        return $event;
    }
}
