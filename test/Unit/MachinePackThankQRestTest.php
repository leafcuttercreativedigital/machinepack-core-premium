<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Crmsync;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackThankQRestTest extends TestCase
{
    /**
     * Test a valid one-off credit card payment
     */
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /* public function testValidProfileCreation()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Intagible/Event.action'] = 'user creation';

        $results     = MachinePack::send(
            'crmsync.create.thankqrest',
            $event
        );
        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'crmsync.create.thankqrest',
            $event
        );

        $firstResult = array_pop($results);
        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $event['Intagible/Event.action']           = 'recurring donation';
        $event['Intangible/ThankQ.nextinstalment'] = date("Y-m-d", strtotime("+1 month", strtotime('now')));
        $event['Intangible/ThankQ.paymentisfirst'] = true;

        $results = MachinePack::send(
            'crmsync.create.thankqrest',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */
    
    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array(
            'Order.identifier' => '29673618',
            'Order.description' => 'marlin admin ezdebit Donation',
            'Order.orderDate' => '2021-08-30',
            'Order.paymentMethod' => 'Credit Card',
            'Order.orderStatus' => 'Ok',
            'MonetaryAmount.currency' => '$ (AUD)',
            'MonetaryAmount.value' => '75.00',
            'Person.honorificPrefix' => '',
            'Person.givenName' => 'marlin',
            'Person.familyName' => 'admin',
            'Person.email' => 'dev@marlincommunications.com',
            'Person.telephone' => '0425132242',
            'Person.birthDate' => '',
            'Organization.name' => '',
            'PostalAddress.streetAddress' => '272 Pacific Highway',
            'PostalAddress.addressLocality' => 'Crows Nest',
            'PostalAddress.addressRegion' => 'NSW',
            'PostalAddress.postalCode' => '2065',
            'PostalAddress.addressCountry' => 'Australia',
            'CreditCard/CardDetails.name' => '',
            'CreditCard/CardDetails.number' => '',
            'CreditCard/CardDetails.expiryMonth' => '',
            'CreditCard/CardDetails.expiryYear' => '',
            'CreditCard/CardDetails.type' => '',
            'CreditCard/CardDetails.token' => '1234',
            'Product.productId' => '',
            'Intangible/ThankQ.startDate' => '2021-08-30',
            'Intangible.contactType' => 'Individual',
            'Intangible/ThankQ.notes' => 'Successful Payment. Transaction ID: 29673618',
            'Intangible/ThankQ.sourceCode' => 'MarlinTest',
            'Intangible/ThankQ.sourceCode2' => 'MarlinTest',
            'Intangible/ThankQ.destinationCode' => 'MarlinTest',
            'Intangible/ThankQ.campaignYear' => '2021',
            'Intangible/ThankQ.nextinstalment' => '2021-09-15',
            'Intangible/ThankQ.paymentisfirst' => '1',
            'Intangible/ThankQ.bequest' => '0',
            'Intagible/Event.action' => 'recurring donation',
            'Intagible/Profile.firstname' => 'Test',
            'Intagible/Profile.lastname' => 'User',
            'Intagible/Profile.type' => 'In Celebration',
            'Intagible/Event.utm_source' => 'test',
            'Intagible/Event.utm_medium' => 'test',
            'Intagible/Event.utm_campaign' => 'test',
            'Intagible/Event.utm_content' => 'test',
            'Intagible/Event.utm_term' => '',
            'Intangible/Event.payload' => '',
        );
        return $event;
    }
}
