<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Exception\Semantic as SemanticException;
use MachinePack\Core\Event\Events\Payment;

final class PremiumHandlersTest extends TestCase
{
    public function testInsightsAIConfigFromFile()
    {
        $this->expectException(SemanticException::class);
        MachinePack::init(__DIR__ . '/PremiumHandlersTest.config.yml');

        $payment_data = [
            'MonetaryAmount.currency' => 'AUD',
            'MonetaryAmount.value' => '1.00'
        ];

        ob_start();
        try {
            MachinePack::send('payment.one-off.success', $payment_data);
        } catch (SemanticException $e) {
            $logger_out = ob_get_contents();
            ob_end_clean();

            $this->assertRegexp('/' . preg_quote(Payment::class) . '.*1.00 AUD/', $logger_out);

            throw $e;
        }
    }
}
