<?php
declare(strict_types=1);

namespace MachinePack\Core\Test\Unit;

use PHPUnit\Framework\TestCase;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Exception\MissingArguments as MissingArgumentsException;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;

final class MachinePackThankQTest extends TestCase
{
    /**
     * Test a valid one-off credit card payment
     */
    public function test()
    {
        $firstResult = new Success(true);

        $this->assertInstanceOf(Success::class, $firstResult);
    }
    /**
     * Test a valid one-off credit card payment
     */
    /* public function testValidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();

        $results = MachinePack::send(
            'payment.create.thankq',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Test an invalid one-off credit card payment
     */
    /* public function testInvalidOneOffPayment()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['CreditCard/CardDetails.number'] = '333322221111';

        $results = MachinePack::send(
            'payment.create.thankq',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Failure::class, $firstResult);
    } */

    /**
     * Test a valid recurring credit card payment
     */
    /* public function testValidSubscription()
    {
        MachinePack::init(__DIR__ . '/MachinePackTest.yml');

        $event = $this->_createValidPaymentEvent();
        $event['Product.productId']             = 'DONONLINE_RECURRING';
        $event['Product/Subscription.duration'] = '31/12/2050';

        $results = MachinePack::send(
            'subscription.create.thankq',
            $event
        );

        $firstResult = array_pop($results);

        $this->assertInstanceOf(Success::class, $firstResult);
    } */

    /**
     * Creates a valid payment event
     */
    private function _createValidPaymentEvent()
    {
        $event = array();
        $event['Order.identifier']            = 11111111;
        $event['Order.description']           = 'Test transaction into eWay';
        $event['Person.givenName']            = 'John';
        $event['Person.familyName']           = 'Doe';
        $event['Person.telephone']            = 0404000000;
        $event['Person.email']                = 'test@leafcutter.com.au';
        $event['Person.ip']                   = '127.0.0.1';
        $event['PostalAddress.streetAddress'] = '123 Nowhere St';
        $event['PostalAddress.addressLocality']      = 'Crows Nest';
        $event['PostalAddress.addressRegion']        = 'NSW';
        $event['PostalAddress.postalCode']           = '2065';
        $event['PostalAddress.addressCountry']       = 'AU';
        $event['CreditCard/CardDetails.name']        = 'John Doe';
        $event['CreditCard/CardDetails.type']        = 'Visa';
        $event['CreditCard/CardDetails.number']      = '4111111111111111';
        $event['CreditCard/CardDetails.expiryMonth'] = '12';
        $event['CreditCard/CardDetails.expiryYear']  = '25';
        $event['CreditCard/CardDetails.cvn']         = '123';
        $event['MonetaryAmount.value']               = 10.00;
        $event['MonetaryAmount.currency']            = 'AUD';
        return $event;
    }
}
