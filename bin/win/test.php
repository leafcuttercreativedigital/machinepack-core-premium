<?php
// Help function to require libriaries in subfolders
require 'require_all.php';

//Import vendor libraries
require './vendor/autoload.php';

//Import local libraries including all subfolders
Require_all('./src/Handler/Crmsync');
Require_all('./src/Handler/Logger');
Require_all('./src/Handler/Payment');
Require_all('./test/');

//Run specific test
$test  = new \MachinePack\Core\Test\Unit\MachinePackEtapestryTest();
$event = $test->testValidRecurringGiftCreation();

die('Done.');
