<?php
function Require_all($path, $depth = 0)
{
    $dirhandle      = @opendir($path);
    $excludeFolders = ['./src/Handler/Payment/xml', './src/Handler/Payment/lib'];
    if (in_array($path, $excludeFolders)) {
        return;
    }
    if ($dirhandle === false) {
        return;
    }
    while (($file = readdir($dirhandle)) !== false) {
        if ($file !== '.' && $file !== '..') {
            $fullfile = $path . '/' . $file;
            if (is_dir($fullfile)) {
                Require_all($fullfile, $depth+1);
            } elseif (strlen($fullfile)>4 && substr($fullfile, -4) == '.php') {
                require $fullfile;
            }
        }
    }

    closedir($dirhandle);
}
