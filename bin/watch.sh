#!/bin/bash

find src/ test/ -name '*.php' \
	| entr bash -c "clear; (vendor/bin/phpcs || vendor/bin/phpcbf) && vendor/bin/phpunit"
