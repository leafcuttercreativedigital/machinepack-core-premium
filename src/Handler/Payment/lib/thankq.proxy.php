<?php
/**
 * This script is a light helper script for testing ThankQ payments on non-Linux environments.
 * This script needs to be placed onto an openly accessible Linux server, and then you need to
 * configure your machinepack to access these files.
 **/


//encryption functions
function encryptKey($companyCode, $gateway)
{
    $lastline     = exec(__DIR__ . "/ThankQEncryption Identity " . $companyCode . " " . $gateway, $outputArray);
    $encryptedKey = implode($outputArray);
    return $encryptedKey;
}

function encryptCardNumber($cardNumber)
{
    $lastline            = exec(__DIR__ . "/ThankQEncryption CreditCard " . $cardNumber, $outputArray);
    $encryptedCardNumber = implode($outputArray);
    return $encryptedCardNumber;
}

//logic for proxy
if ($_GET['passcode']!='afskh140986au') {
    die('Cannot access proxy');
}

if ($_GET['action'] == 'Identity') {
    echo encryptKey($_GET['companyCode'], $_GET['gateway']);
}

if ($_GET['action'] == 'CreditCard') {
    echo encryptCardNumber($_GET['cardNumber']);
}

//if no action found, just die
die();
