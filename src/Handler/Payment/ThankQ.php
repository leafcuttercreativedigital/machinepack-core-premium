<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;

class ThankQ extends Handler
{
    const EMAIL_FIELD = 'Person.email';

    const API_URL_SANDBOX     = 'https://thankqportal.com/eModulesAPI_TEST/API.asmx';
    const API_URL_PRODUCTION  = 'https://thankqportal.com/eModulesAPI/API.asmx';
    const DEFAULT_SOURCE_CODE = 'DONONLINE';

    private $_config;

    private $_sessionToken;
    private $_sourceCode;
    private $_encryptionKey;
    private $_encryptedCardNumber;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Configuration details can be either passed as args or loaded from config
        if (empty($event['Intangible/ThankQ.companyCode'])
            || empty($event['Intangible/ThankQ.onceOffGateway'])
            || empty($event['Intangible/ThankQ.recurringGateway'])
        ) {
            //validate configuration variables
            if (empty($this->_config['companyCode'])
                || empty($this->_config['onceOffGateway'])
                || empty($this->_config['recurringGateway'])
            ) {
                return new Failure(
                    'Please add handler settings for ThankQ. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                companyCode: "...",
                                onceOffGateway: "...",
                                recurringGateway: "...",
                                isProductionMode: "..."
                            }
                        }
                    }
                '
                );
            }
        } else {
            $this->_config['companyCode']      = $event['Intangible/ThankQ.companyCode'];
            $this->_config['onceOffGateway']   = $event['Intangible/ThankQ.onceOffGateway'];
            $this->_config['recurringGateway'] = $event['Intangible/ThankQ.recurringGateway'];
            $this->_config['isProductionMode'] = isset($event['Intangible/ThankQ.isProductionMode'])
                ? $event['Intangible/ThankQ.isProductionMode'] : false;
        }

        //TODO check if linux env - only runs in linux
 
        //set source code
        if (isset($event['Intangible/ThankQ.sourceCode'])) {
            $this->_sourceCode = $event['Intangible/ThankQ.sourceCode'];
        } else {
            $this->_sourceCode = self::DEFAULT_SOURCE_CODE;
        }

        //depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processSubscription($event);
        }

        return $this->_processPayment($event);
    }

    /**
     * Handle a single payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processPayment(Payment $event)
    {
        try {
            $paymentReference = $this->_generatePayment($event);

            $donation = $this->_submitOneOffPayment($this->_sessionToken, $paymentReference, $event);

            $responseData = $this->_capture($event, $paymentReference);
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success($responseData);
    }


    /**
     * Handle a recurring payment request
     * @param  Payment $event    payment information
     * @return Success|Failure
     */
    private function _processSubscription(Payment $event)
    {
        try {
            $paymentReference = $this->_generatePayment($event);

            $recurringDonation = $this->_submitMonthlyPayment($this->_sessionToken, $paymentReference, $event);

            $donation = $this->_submitOneOffPayment($this->_sessionToken, $paymentReference, $event);

            $responseData = $this->_capture($event, $paymentReference);
        } catch (\Exception $e) {
            return new Failure(
                $e->getMessage()
            );
        }

        return new Success($responseData);
    }

     /**
      * Handle a single payment request
      * @param  Payment $event    payment information
      * @return Success|Failure
      */
    private function _generatePayment(Payment $event)
    {
        //Generate ThankQ session token
        if (isset($event['Intangible/onceOffGateway'])) {
            $companyCode = null;
            if (isset($event['Intangible/companyCode'])) {
                $companyCode = $event['Intangible/companyCode'];
            }
            $this->_sessionToken = $this->_generateSessionToken($event['Intangible/onceOffGateway'], $companyCode);
        } else {
            $this->_sessionToken = $this->_generateSessionToken($this->_config['onceOffGateway']);
        }

        //Encrypt Card Number
        $this->_encryptedCardNumber = $this->_encryptCardNumber(
            str_replace(' ', '', $event['CreditCard/CardDetails.number'])
        );

        //Create Contact
        $contactSerialNumber = $this->_createContact($this->_sessionToken, $event);

        //Create Payment
        $paymentReference = $this->_createPayment($this->_sessionToken, $contactSerialNumber);

        return $paymentReference;
    }

    private function _generateSessionToken($gateWayId, $companyCode = null)
    {
        if (!$companyCode) {
            $companyCode = $this->_config['companyCode'];
        }
        if ($this->_config['useEncryptionProxy']) {
            $this->_encryptionKey = $this->_proxyCurl(
                sprintf(
                    '?passcode=afskh140986au&action=Identity&companyCode=%s&gateway=%s',
                    $companyCode,
                    $gateWayId
                )
            );
        } else {
            //generate the key by running shell command
            $outputArray          = array();
            $lastline             = exec(
                sprintf(
                    '%s/lib/ThankQEncryption Identity %s %s',
                    escapeshellarg(__DIR__),
                    escapeshellarg($companyCode),
                    escapeshellarg($gateWayId)
                ),
                $outputArray
            );
            $this->_encryptionKey = implode($outputArray);
        }

        //Create session
        return $this->_createSession($this->_encryptionKey);
    }

    private function _createSession($encryptedKey)
    {
        // xml post structure
        $xml_post_string = require __DIR__ . '/xml/createSession.xml.php';
        $headers         = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/CreateSession",
            "Content-length: ".strlen($xml_post_string)
        ); //SOAPAction: your op URL

        $parser = $this->_curl($headers, $xml_post_string);

        // use $parser to get your data out of XML response and to display it.
        return $parser->CreateSessionResponse->CreateSessionResult->__toString();
    }

    private function _encryptCardNumber($cardNumber)
    {
        if ($this->_config['useEncryptionProxy']) {
            return $this->_proxyCurl('?passcode=afskh140986au&action=CreditCard&cardNumber='. $cardNumber);
        } else {
            $outputArray         = array();
            $lastline            = exec(
                sprintf(
                    '%s/lib/ThankQEncryption CreditCard %s',
                    escapeshellarg(__DIR__),
                    escapeshellarg($cardNumber)
                ),
                $outputArray
            );
            $encryptedCardNumber = implode($outputArray);
            return $encryptedCardNumber;
        }
    }

    private function _createContact($sessionToken, $event)
    {
        // xml post structure
        $xml_post_string = require __DIR__ . '/xml/createContact.xml.php';
        $headers         = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/AddContact",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);

        // user $parser to get your data out of XML response and to display it.
        return $parser->AddContactResponse->AddContactResult;
    }

    private function _createPayment($sessionToken, $contactSerialNumber)
    {
        $date_of_payment = (new \DateTime())->format('c');
        $xml_post_string = require __DIR__ . '/xml/createPayment.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/AddPayment",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);

        // user $parser to get your data out of XML response and to display it.
        return $parser->AddPaymentResponse->AddPaymentResult;
    }

    private function _submitMonthlyPayment($sessionToken, $paymentReference, $event)
    {
        $payment_amount  = str_replace('$', '', $event['MonetaryAmount.value']);
        $source_code     = $this->_sourceCode;
        $xml_post_string = require __DIR__ . '/xml/submitMonthlyPayment.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/AddPaymentPledge",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);
        // user $parser to get your data out of XML response and to display it.
        return $parser->AddPaymentPledgeResponse->AddPaymentPledgeResult;
    }

    private function _submitOneOffPayment($sessionToken, $paymentReference, $event)
    {
        $payment_amount     = str_replace('$', '', $event['MonetaryAmount.value']);
        $source_code        = $this->_sourceCode;
        $fornameaddress     = sprintf(
            '%s %s %s %s %s',
            $event['PostalAddress.streetAddress'],
            $event['PostalAddress.addressLocality'],
            $event['PostalAddress.postalCode'],
            $event['PostalAddress.addressRegion'],
            $event['PostalAddress.addressCountry']
        );
        $fornamecelebration = sprintf('%s %s', $event['Person.givenName'], $event['Person.familyName']);
        $xml_post_string    = require __DIR__ . '/xml/submitOneOffPayment.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/AddPaymentDonation",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);

        // use $parser to get your data out of XML response and to display it.
        return $parser->AddPaymentDonationResponse->AddPaymentDonationResult;
    }

    private function _capture($event, $paymentReference)
    {
        $submitPaymentDetails = $this->_submitPaymentDetails(
            $this->_sessionToken,
            $paymentReference,
            $this->_encryptedCardNumber,
            $event
        );

        $closed = $this->_closeSession($this->_sessionToken);

        $dom = new \DOMDocument();
        $dom->loadXML($submitPaymentDetails->asXML());

        if ($dom->getElementsByTagName('ProcessPaymentDetailsResponse')
            && $dom->getElementsByTagName('ProcessPaymentDetailsResponse')->length > 0
        ) {
            $donationDetailsXML = $this->_getPaymentDetails($this->_encryptionKey, $paymentReference);
            $paymentDetailsXML  = $this->_getPayment($this->_encryptionKey, $paymentReference);

            $responseData = [
                'webauthorisation'  => (string) $donationDetailsXML->PaymentDetails->webauthorisation,
                'webReference'      => (string) $donationDetailsXML->PaymentDetails->webreference,
                'serialNumber'      => (string) $paymentDetailsXML->Payment->serialnumber,
                'dateofpayment'     => (string) $paymentDetailsXML->Payment->dateofpayment,
            ];

            return $responseData;
        }

        //if we get here, we havent captured the details correctly
        throw new \Exception($dom->getElementsByTagName('errormessage')->item(0)->nodeValue);
    }

    private function _submitPaymentDetails($sessionToken, $paymentReference, $encryptedCard, $event)
    {
        //FIXME this should be processed better to stop issue occurring
        $year[]           = substr($event['CreditCard/CardDetails.expiryYear'], -2);
        $creditcardexpiry = $event['CreditCard/CardDetails.expiryMonth'] . '/' . $year[0];
        $xml_post_string  = require __DIR__ . '/xml/submitPaymentDetails.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/ProcessPaymentDetails",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);

        return $parser;
    }

    private function _closeSession($sessionToken)
    {
        // xml post structure
        $xml_post_string = require __DIR__ . '/xml/closeSession.xml.php';
        $headers         = array(
            "Content-type: text/xml;charset=\"utf-8\"",
            "SOAPAction: http://thankqportal.com/eModulesAPI/CloseSession",
            "Content-length: ".strlen($xml_post_string)
        );

        $parser = $this->_curl($headers, $xml_post_string);
        // use $parser to get your data out of XML response and to display it.
        return $parser->CloseSessionResponse->CloseSessionResult->__toString;
    }

    private function _getPaymentDetails($encryptedKey, $webReference)
    {
        // xml post structure
        $xml_post_string = require __DIR__ . '/xml/getPaymentDetails.xml.php';
        $headers         = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/GetPaymentDetails",
            "Content-length: ".strlen($xml_post_string)
        );


        $parser = $this->_curl($headers, $xml_post_string);
        // use $parser to get your data out of XML response and to display it.
        return $parser->GetPaymentDetailsResponse->GetPaymentDetailsResult;
    }

    private function _getPayment($encryptedKey, $webReference)
    {
        // xml post structure
        $xml_post_string = require __DIR__ . '/xml/getPayment.xml.php';

        $headers = array(
            "Content-type: text/xml;charset=UTF-8",
            "SOAPAction: http://thankqportal.com/eModulesAPI/GetPayment",
            "Content-length: ".strlen($xml_post_string)
        );


        $parser = $this->_curl($headers, $xml_post_string);
        // user $parser to get your data out of XML response and to display it.
        return $parser->GetPaymentResponse->GetPaymentResult;
    }

    private function _curl($headers, $xml_post_string)
    {
        // PHP cURL
        $ch = curl_init($this->_getApiUrl());

        if (false === $ch) {
            throw new \Exception('Unable to initialise curl.');
        }

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_post_string);

        // converting
        $response = curl_exec($ch);

        if (false === $response) {
            throw new \Exception(
                sprintf(
                    'Payment system appears down. Errno %s (%s).',
                    curl_errno($ch),
                    curl_error($ch)
                )
            );
        }

        curl_close($ch);

        // converting
        $response1 = str_replace("<soap:Body>", "", $response);
        $response2 = str_replace("</soap:Body>", "", $response1);

        // converting to XML
        $parser = simplexml_load_string($response2);

        return $parser;
    }

    private function _proxyCurl($queryParams)
    {
        // PHP cURL
        $fullUrl = $this->_config['encryptionProxyUrl'] . $queryParams;

        $ch = curl_init($fullUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // converting
        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

    /**
     * Get the API URL based on environment
     * @return string
     */
    private function _getApiUrl():string
    {
        if ($this->_config['isProductionMode']) {
            return self::API_URL_PRODUCTION;
        } else {
            return self::API_URL_SANDBOX;
        }
    }

    public function getPaymentSettings()
    {
        return [
            'thankq' => $this->settings[$this->settings['env']]
        ];
    }
}
