<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:emod="http://thankqportal.com/eModulesAPI">
	<soapenv:Header/>
	<soapenv:Body>
		<emod:AddPaymentPledge>
			<emod:token>$sessionToken</emod:token>
			<emod:pledgeXml>
				<pay:Pledge xmlns:pay="http://thankqportal.com/eModulesAPI/Payment.xsd">
					<pay:paymentreference>$paymentReference</pay:paymentreference>
					<pay:paymentamount>$payment_amount</pay:paymentamount>
					<pay:pledgeplan>Continuous</pay:pledgeplan>
					<pay:paymentfrequency>Monthly</pay:paymentfrequency>
					<pay:sourcecode>$source_code</pay:sourcecode>
					<pay:sourcecode2>$source_code</pay:sourcecode2>
					<pay:thispaymentisfirstinstalment>-1</pay:thispaymentisfirstinstalment>
				</pay:Pledge>
			</emod:pledgeXml>
		</emod:AddPaymentPledge>
	</soapenv:Body>
</soapenv:Envelope>
XML;
