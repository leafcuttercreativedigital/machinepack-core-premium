<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	 <soap:Body>
			 <AddContact xmlns="http://thankqportal.com/eModulesAPI">
					 <token>$sessionToken</token>
					 <contactXml>
							 <con:Contact xmlns:con="http://thankqportal.com/eModulesAPI/Contact.xsd">
									 <con:firstname>{$event['Person.givenName']}</con:firstname>
									 <con:keyname>{$event['Person.familyName']}</con:keyname>
									 <con:postcode>{$event['PostalAddress.postalCode']}</con:postcode>
									 <con:addressline1>{$event['PostalAddress.streetAddress']}</con:addressline1>
									 <con:suburb>{$event['PostalAddress.addressLocality']}</con:suburb>
									 <con:state>{$event['PostalAddress.addressRegion']}</con:state>
									 <con:country>{$event['PostalAddress.addressCountry']}</con:country>
									 <con:mobilenumber>{$event['Person.telephone']}</con:mobilenumber>
									 <con:emailaddress>{$event['Person.email']}</con:emailaddress>
							 </con:Contact>
					 </contactXml>
			 </AddContact>
	 </soap:Body>
</soap:Envelope>
XML;
