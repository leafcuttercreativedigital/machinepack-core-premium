<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:emod="http://thankqportal.com/eModulesAPI">
	<soapenv:Header/>
	<soapenv:Body>
		<emod:AddPaymentDonation>
			<!--Optional:-->
			<emod:token>$sessionToken</emod:token>
			<!--Optional:-->
			<emod:donationXml>
				<don:Donation xmlns:don="http://thankqportal.com/eModulesAPI/Payment.xsd">
					<don:paymentreference>$paymentReference</don:paymentreference>
					<don:paymentamount>$payment_amount</don:paymentamount>
					<don:fornamecelebration>$fornamecelebration</don:fornamecelebration>
					<don:fornameaddress>$fornameaddress</don:fornameaddress>
					<don:sourcecode>$source_code</don:sourcecode>
					<don:sourcecode2>$source_code</don:sourcecode2>
				</don:Donation>
			</emod:donationXml>
		</emod:AddPaymentDonation>
	</soapenv:Body>
</soapenv:Envelope>
XML;
