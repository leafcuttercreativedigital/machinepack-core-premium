<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:emod="http://thankqportal.com/eModulesAPI">
	<soapenv:Header/>
	<soapenv:Body>
		<emod:GetPayment>
			<!--Optional:-->
			<emod:encryptedKey>$encryptedKey</emod:encryptedKey>
			<!--Optional:-->
			<emod:webReference>$webReference</emod:webReference>
		</emod:GetPayment>
	</soapenv:Body>
</soapenv:Envelope>
XML;
