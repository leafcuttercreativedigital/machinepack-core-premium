<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xsd="http://www.w3.org/2001/XMLSchema"
	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
	<soap:Body>
		<AddPayment xmlns="http://thankqportal.com/eModulesAPI">
			<token>$sessionToken</token>
			<paymentXml>
				<pay:Payment xmlns:pay="http://thankqportal.com/eModulesAPI/Payment.xsd">
					<pay:serialnumber>$contactSerialNumber</pay:serialnumber>
					<pay:dateofpayment>$date_of_payment</pay:dateofpayment>
					<pay:receiptrequired>Yes</pay:receiptrequired>
					<pay:receiptsummary>Yes</pay:receiptsummary>
				</pay:Payment>
			</paymentXml>
		</AddPayment>
	</soap:Body>
</soap:Envelope>
XML;
