<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
    <soapenv:Envelope
        xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
        xmlns:emod="http://thankqportal.com/eModulesAPI">
    <soapenv:Header/>
    <soapenv:Body>
        <emod:CreateSession>
            <!--Optional:-->
            <emod:encryptedKey>$encryptedKey</emod:encryptedKey>
            <emod:duration>15</emod:duration>
            <emod:persisting>true</emod:persisting>
        </emod:CreateSession>
    </soapenv:Body>
</soapenv:Envelope>
XML;
