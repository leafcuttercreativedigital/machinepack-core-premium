<?php return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<soapenv:Envelope
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:emod="http://thankqportal.com/eModulesAPI">
	<soapenv:Header/>
	<soapenv:Body>
		<emod:ProcessPaymentDetails>
			<!--Optional:-->
			<emod:token>$sessionToken</emod:token>
			<!--Optional:-->
			<emod:paymentDetailsXml>
				<pay:PaymentDetails xmlns:pay="http://thankqportal.com/eModulesAPI/Payment.xsd">
					<pay:webreference>$paymentReference</pay:webreference>
					<pay:paymenttype>Credit Card</pay:paymenttype>
                    <pay:ipaddress>{$event['Person.ip']}</pay:ipaddress>
					<pay:creditcarddetails>
						<pay:creditcardtype>{$event['CreditCard/CardDetails.type']}</pay:creditcardtype>
						<pay:creditcardnumber>$encryptedCard</pay:creditcardnumber>
						<pay:creditcardholder>{$event['CreditCard/CardDetails.name']}</pay:creditcardholder>
						<pay:creditcardexpiry>$creditcardexpiry</pay:creditcardexpiry>
						<creditcardissuenumber>{$event['CreditCard/CardDetails.cvn']}</creditcardissuenumber>
					</pay:creditcarddetails>
				</pay:PaymentDetails>
			</emod:paymentDetailsXml>
		</emod:ProcessPaymentDetails>
	</soapenv:Body>
</soapenv:Envelope>
XML;
