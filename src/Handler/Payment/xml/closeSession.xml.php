<?php return <<<XML
<?xml version ="1.0" encoding="utf-8"?>
<soapenv:Envelope
	xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
	xmlns:emod="http://thankqportal.com/eModulesAPI">
	<soapenv:Header/>
	<soapenv:Body>
		<emod:CloseSession>
			<!--Optional:-->
			<emod:token>$sessionToken</emod:token>
		</emod:CloseSession>
	</soapenv:Body>
</soapenv:Envelope>
XML;
