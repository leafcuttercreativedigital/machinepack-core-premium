<?php

namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Result;

class CauseView extends Handler
{
    private $_config;
    const API_URL = 'https://api.causeview.com/v2.1/services/rest/form/';

    /**
     * Main method handler
     *
     * @param \MachinePack\Core\Event\Event $event
     * @return Failure|Ignored|Success
     */
    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {

        if (!$event instanceof Payment) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if (empty($this->_config['authentication_code'])) {
            return new Failure(
                'Please add authentication code for causeview. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                authentication_code: 
                            }
                        }
                    }
                '
            );
        }

        $record = $this->_submitToCauseView($event);
        return $record;
    }

    /**
     * Validating the request and calling the remote server
     *
     * @param $event
     * @return bool|false|Failure|mixed|string
     */
    private function _submitToCauseView($event)
    {
        if (isset($event['Intangible/Event.payload'][0]['action_page_id'])
            && !empty($event['Intangible/Event.payload'][0]['action_page_id'])
        ) {
            $api_url = self::API_URL . $event['Intangible/Event.payload'][0]['action_page_id'];
        } else {
            $api_url = $this->_config['api_url'];
        }

        try {
            if ($this->_config['api_url']) {
                $record = $this->_curl($event['method'], $api_url, $event['Intangible/Event.payload']);
                $record = json_decode($record);
                if (isset($record[0]->status) && $record[0]->status == 'success') {
                    return new Success(['Intangible/Event.payload' => $record]);
                } else {
                    return new Failure('Error while processing transaction.', $record);
                }
            } else {
                return new Failure(
                    'You must set the `api_url` property in the event array '
                );
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            //Logger::log(E_STRICT, $message, $e->getTrace());
            return new Failure($message);
        }
    }

    /**
     * Sending curl request to the server and getting the result
     *
     * @param string $method
     * @param string $url
     * @param array $data
     * @return bool|false|string
     */
    private function _curl(string $method, string $url, array $data)
    {
        try {
            $headers    = [
                "Content-Type: text/json",
                "Authorization: Bearer " . $this->_config['authentication_code']
            ];
            $input_data = json_encode($data);

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);

            curl_close($curl);

            return $result;
        } catch (Exception $e) {
            return json_encode(['Errors' => true, 'message' => $e->getMessage()]);
        }
    }
}
