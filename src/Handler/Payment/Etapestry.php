<?php
namespace MachinePack\Core\Handler\Payment;

use MachinePack\Core\Event\Events\Crmsync;
use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Payment;
use MachinePack\Core\Event\Events\Subscription;
use SoapClient;
use SoapHeader;
use nusoap_client;

class Etapestry extends Handler
{

    private $_config;

    private $_nsc;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Payment && ! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //Api credentials can be either passed as args or loaded from config
        if (empty($event['Intangible/Etapestry.database_id'])
            || empty($event['Intangible/Etapestry.api_key'])
        ) {
            //validate configuration variables
            if (empty($this->_config['databaseId']) || empty($this->_config['apiKey'])) {
                MachinePack::log('Please add handler settings for Etapestry.', 'debug');
                return new Failure(
                    'Please add handler settings for Etapestry. Full config should be:
                        {
                            config: {
                                env: <someenv>
                                <someenv>: {
                                    
                                }
                            }
                        }
                    '
                );
            }
        } else {
            $this->_config['databaseId'] = $event['Intangible/Etapestry.database_id'];
            $this->_config['apiKey']     = $event['Intangible/Etapestry.api_key'];
        }

        if ($event instanceof Crmsync) {
            return $this->_processCrmSubmission($event);
        }

        //depending on one-off or recurring, make payment
        if ($event instanceof Subscription) {
            return $this->_processSubscription($event);
        }
        return $this->_processPayment($event);
    }

    /**
     * Handle a crm submission
     * @param Payment $event
     */
    // phpcs:disable
    private function _processCrmSubmission(Crmsync $event)
    {
        switch ($event['Intangible/Etapestry.action']) {
            case 'createContactAndGift':
                return $this->_createContactAndGift($event);
            case 'createContactAndRecurringGift':
                return $this->_createContactAndRecurringGift($event);
        }

        return new Failure('Unhandled Intangible/Etapestry.action');
    }
    // phpcs:enable

    /**
     * Create contact and gift
     * @param $event
     * @return array|bool|Failure
     */
    private function _createContactAndGift($event)
    {
        try {
            // Instantiate nusoap_client and call login method
            $this->_nsc = $this->_startEtapestrySession();
            if ($this->_nsc instanceof Failure) {
                return $this->_nsc;
            }

            //Find or Create Contact
            $contact = $this->_findCreateContact($event);
            if ($contact instanceof Failure) {
                return $contact;
            }

            //Gift card request
            $giftRequest = $this->_generateGiftRequest($event, $contact['referenceId']);
            $response    = $this->_nsc->call("addGift", array($giftRequest, false));
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);

            // Call logout method
            $this->_stopEtapestrySession($this->_nsc);

            if ($status instanceof Failure) {
                return $status;
            } else {
                return new Success(
                    [
                        'Person.identifier'=>$contact['referenceId'],
                        'Contact.identifier'=>$contact['id'],
                        'Receipt.id' => $response
                    ]
                );
            }
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    private function _createContactAndRecurringGift($event)
    {
        try {
            // Instantiate nusoap_client and call login method
            $this->_nsc = $this->_startEtapestrySession();
            if ($this->_nsc instanceof Failure) {
                return $this->_nsc;
            }

            //Find or Create Contact
            $contact = $this->_findCreateContact($event);
            if ($contact instanceof Failure) {
                return $contact;
            }

            //Generate a Recurring Payment Schedule
            $recurringGiftSchedule = $this->_generateRecurringGiftSchedule($event, $contact['referenceId']);
            $response              = $this->_nsc->call(
                "addRecurringGiftSchedule",
                array($recurringGiftSchedule, false)
            );
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }
            // If $response ok, then it will be a recurringGiftScheduleId
            $recurringGiftScheduleId = $response;

            $gift = $this->_nsc->call("getRecurringGiftSchedule", array($recurringGiftScheduleId));
            MachinePack::log($gift, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            $gift['receipt'] = $event['Intangible/receipt.no'] ?? uniqid();
            $gift['note']    = "Processed by " . ($event['Intangible/Gateway.name'] ?? '') . " gateway.";
            $response        = $this->_nsc->call("updateRecurringGiftSchedule", array($gift, false));
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            // Call logout method
            $this->_stopEtapestrySession($this->_nsc);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        //otherwise, payment was taken, return success
        return new Success(
            [
                'Person.identifier'=>$contact['referenceId'],
                'Contact.identifier'=>$contact['id'],
                //TODO Pass this
                //'Receipt.id' => $gift['receipt']
            ]
        );
    }

    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processPayment(Payment $event)
    {
        try {
            // Instantiate nusoap_client and call login method
            $this->_nsc = $this->_startEtapestrySession();
            if ($this->_nsc instanceof Failure) {
                return $this->_nsc;
            }

            //Find or Create Contact
            $contact = $this->_findCreateContact($event);
            if ($contact instanceof Failure) {
                return $contact;
            }

            //  One time payment code runs everytime regardless of payment type
            //  Runs for both OneTime and Recurring Payment
            //  Payment request
            $paymentRequest = $this->_generatePaymentRequest($event, $contact['referenceId']);
            $response       = $this->_nsc->call("addAndProcessGift", array($paymentRequest, false));
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            // If $response ok, then it will be a transactionId for OneTime or Recurring payment
            $transactionId = $response;

            $gift = $this->_nsc->call("getGift", array($transactionId));
            MachinePack::log($gift, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            $gift['receipt'] = isset($event['Intangible/receipt.no'])?$event['Intangible/receipt.no']:uniqid();

            $response = $this->_nsc->call("updateGift", array($gift, false));
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            // Call logout method
            $this->_stopEtapestrySession($this->_nsc);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        //otherwise, payment was taken, return success
        return new Success(
            [
                'MoneyTransfer.identifier'=>$transactionId,
                'Person.identifier'=>$contact['referenceId'],
                'Contact.identifier'=>$contact['id'],
                'Receipt.id' => $gift['receipt']
            ]
        );
    }

    /**
     * Handle a payment request (recurring or one off)
     * @param  Payment $event       payment information
     * @param  boolean $recurring   recurring payment flag
     * @return Success|Failure
     */
    private function _processSubscription(Payment $event)
    {
        try {
            // Instantiate nusoap_client and call login method
            $this->_nsc = $this->_startEtapestrySession();
            if ($this->_nsc instanceof Failure) {
                return $this->_nsc;
            }

            //Find or Create Contact
            $contact = $this->_findCreateContact($event);
            if ($contact instanceof Failure) {
                return $contact;
            }

            //Generate a Recurring Payment Schedule
            $recurringGiftSchedule = $this->_generateRecurringGiftSchedule($event, $contact['referenceId']);
            $response              = $this->_nsc->call(
                "addRecurringGiftSchedule",
                array($recurringGiftSchedule, false)
            );
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }
            // If $response ok, then it will be a transactionId
            $transactionId = $response;

            $gift = $this->_nsc->call("getRecurringGiftSchedule", array($transactionId));
            MachinePack::log($gift, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            $gift['receipt'] = isset($event['Intangible/receipt.no'])?$event['Intangible/receipt.no']:uniqid();
            $response        = $this->_nsc->call("updateRecurringGiftSchedule", array($gift, false));
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            // Call logout method
            $this->_stopEtapestrySession($this->_nsc);
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }

        //otherwise, payment was taken, return success
        return new Success(
            [
                'MoneyTransfer.identifier'=>$transactionId,
                'Person.identifier'=>$contact['referenceId'],
                'Contact.identifier'=>$contact['id'],
                'Receipt.id' => $gift['receipt']
            ]
        );
    }

    /**
     * Find or create Contact
     * @param $event
     * @return array|bool|Failure
     */
    private function _findCreateContact($event)
    {
        $response    = null;
        $contactId   = '';
        $searchValue = array(
            'email'     => $event['Person.email'],
            'allowEmailOnlyMatch' => true
        );

        // Invoke getDuplicateAccount method
        $response = $this->_nsc->call("getDuplicateAccount", array($searchValue));
        $status   = $this->_checkStatus($this->_nsc);

        if ($response == null || isset($response['faultcode'])) {
            $accountRequest = $this->_generateAccountRequest($event);
            $response       = $this->_nsc->call("addAccount", array($accountRequest, false));
            MachinePack::log($response, 'debug');
            $status = $this->_checkStatus($this->_nsc);
            if ($status instanceof Failure) {
                return $status;
            }
            $referenceId = $response;
        } else {
            $referenceId = $response['ref'];
            $contactId   = $response['id'];

            if (isset($event['Intangible/Contact.ifExist']) && $event['Intangible/Contact.ifExist'] === 'update') {
                $accountUpdateRequest = $this->_generateAccountUpdateRequest($event, $referenceId);
                $updateresponse       = $this->_nsc->call("updateAccount", array($accountUpdateRequest, false));

                MachinePack::log($updateresponse, 'debug');
                $status = $this->_checkStatus($this->_nsc);
                if ($status instanceof Failure) {
                    return $status;
                }
            }
        }

        if (isset($event['Intangible/Affiliation.type']) && $event['Intangible/Affiliation.type']) {
            $definedValues = array(
                'fieldName' => 'Affiliation',
                'value'     => $event['Intangible/Affiliation.type'],
            );

            $response = $this->_nsc->call("applyDefinedValues", array($referenceId, array($definedValues), false));
            $status   = $this->_checkStatus($this->_nsc);
        }

        return [
            'id'          => $contactId,
            'referenceId' => $referenceId,
            'response'    => $response,
            'status'      => $status
        ];
    }

    /**
     * Generate an add account request for etapestry soap client
     *
     * @param array $event
     * @return void
     */
    private function _generateAccountRequest($event)
    {
        $payload = [
            "nameFormat" => 1,
            "name" => $event['Person.givenName'].' '.$event['Person.familyName'],
            "sortName" => $event['Person.familyName'].', '.$event['Person.givenName'],
            "firstName" => $event['Person.givenName'],
            "lastName" => $event['Person.familyName'],
            "personaType" => $event['Person.personaType'] ?? 'Personal',
            "email" => $event['Person.email'],
            "address" => $event['PostalAddress.streetAddress'],
            "suburb" => $event['PostalAddress.addressLocality'], //Not available outside AU/NZ
            "state" => $event['PostalAddress.addressRegion'], //Not available for certain states
            "postalCode" => $event['PostalAddress.postalCode'],
            "country" => $event['PostalAddress.addressCountry'],
            "shortSalutation" => $event['Person.shortSalutation'] ?? '',
            "longSalutation" => $event['Person.longSalutation'] ?? '',
            "envelopeSalutation" => $event['Person.envelopeSalutation'] ?? '',
            "phones" => array(
                array(
                    'type' => 'Mobile',
                    'number' => $event['Person.telephone'],
                )
            )
        ];

        //Include accountDefinedFields if provided
        if (!empty($event['Person.accountDefinedFields'])) {
            $accountDefinedValues = array();

            foreach ($event['Person.accountDefinedFields'] as $key => $value) {
                $accountDefinedValues[] = array(
                    'fieldName' => $key,
                    'value' => $value,
                );
            }

            if (!empty($accountDefinedValues)) {
                $payload['accountDefinedValues'] = $accountDefinedValues;
            }
        }

        return $payload;
    }

    /**
     * Generate an update account request for etapestry soap client
     *
     * @param array $event
     * @return void
     */
    private function _generateAccountUpdateRequest($event, $referenceId)
    {
        //Use the addAccount event
        $payload = $this->_generateAccountRequest($event);

        //Add ref
        $payload["ref"] = $referenceId;

        return $payload;
    }

    /**
     * Define Recurring Gift Schedule
     * @param array $event
     * @param string $referenceId
     * @return void
     */
    private function _generateRecurringGiftSchedule($event, $referenceId)
    {
        $orderFrequency = (!empty($event['Product/Subscription.frequency']) ?
            $event['Product/Subscription.frequency'] : 12 );

        $firstInstallmentDate = $this->_getFirstInstallmentDate($event);
        $scheduleProcessType  = 1;
        if (isset($event['Intangible/Etapestry.action'])
            && $event['Intangible/Etapestry.action'] == 'createContactAndRecurringGift'
        ) {
            $scheduleProcessType = 0;
        }

        $params = [
            "accountRef" => $referenceId,
            "fund"  => isset($event['Intangible/Gateway.fund']) ?
                $event['Intangible/Gateway.fund'] : "General",
            "amount" => $event['MonetaryAmount.value'],
            "receipt" => uniqid(),
            "schedule" => [
                "processType" => $scheduleProcessType, // Auto
                "frequency" => $orderFrequency,
                "firstInstallmentDate" => $firstInstallmentDate->format("Y-m-d\TH:i:sP"),
                //Etapestry may be using Washington DC timezone GMT-4
                //The problem is that if the date in AU doesn't match the date in Washington DC (previous day)
                //then it throws errors. So better to add certain number of hours to AU time
                //to have the same USA date. Hours doesn't matter anyway, payments are being processed on date
                "installmentAmount" => $event['MonetaryAmount.value']
            ],
        ];

        //Campaign and Approach are not required
        if (!empty($event['Intangible/Gateway.campaign'])) {
            $params['campaign'] = $event['Intangible/Gateway.campaign'];
        }

        if (!empty($event['Intangible/Gateway.approach'])) {
            $params['approach'] = $event['Intangible/Gateway.approach'];
        }

        if ($scheduleProcessType === 1) {
            $params["scheduledValuable"] = [
                "type" => 3,
                "creditCard"=> [
                    "nameOnCard" => $event['CreditCard/CardDetails.name'],
                    "number" => $event['CreditCard/CardDetails.number'],
                    "expirationMonth" => $event['CreditCard/CardDetails.expiryMonth'],
                    "expirationYear" => $event['CreditCard/CardDetails.expiryYear']
                ]
            ];
        }

        if (!empty($event['Intangible/Gift.final']) && $event['Intangible/Gift.final'] !== 'false') {
            $params['final'] = true;
        }

        return $params;
    }

    /**
     * Generate a gift request for etapestry soap client
     *
     * @param $event
     * @param $referenceId
     */
    private function _generateGiftRequest($event, $referenceId)
    {
        $request = [
            "accountRef" => $referenceId,
            "fund"  => isset($event['Intangible/Gateway.fund']) ?
                $event['Intangible/Gateway.fund'] : "General",
            "campaign" => isset($event['Intangible/Gateway.campaign']) ?
                $event['Intangible/Gateway.campaign'] : "Website Donation",
            "approach" => isset($event['Intangible/Gateway.approach']) ?
                $event['Intangible/Gateway.approach'] : "Online Donation",
            "amount" => $event['MonetaryAmount.value'],
            "receipt" => $event['Intangible/receipt.no'] ?? uniqid(),
            "note" => "Processed by " . ($event['Intangible/Gateway.name'] ?? "Unknown source") . " gateway."
        ];

        if (!empty($event['Intangible/Gift.final']) && $event['Intangible/Gift.final'] !== 'false') {
            $request['final'] = true;
        }

        return $request;
    }

    /**
     * Generate a payment request for etapestry soap client
     *
     * @param array $event
     * @param string $referenceId
     * @return void
     */
    private function _generatePaymentRequest($event, $referenceId)
    {
        return [
            "accountRef" => $referenceId,
            "fund"  => isset($event['Intangible/Gateway.fund']) ?
                $event['Intangible/Gateway.fund'] : "General",
            "campaign" => isset($event['Intangible/Gateway.campaign']) ?
                $event['Intangible/Gateway.campaign'] : "Website Donation",
            "approach" => isset($event['Intangible/Gateway.approach']) ?
                $event['Intangible/Gateway.approach'] : "Online Donation",
            "amount" => $event['MonetaryAmount.value'],
            "definedValues"=>[],
            "valuable"=>[
                "type"=>3,
                "creditCard"=> [
                    "nameOnCard" => $event['CreditCard/CardDetails.name'],
                    "number" => $event['CreditCard/CardDetails.number'],
                    "expirationMonth" => $event['CreditCard/CardDetails.expiryMonth'],
                    "expirationYear" => $event['CreditCard/CardDetails.expiryYear']
                ]
            ]
        ];
    }

    /**
     * Get payment settings
     * @return array $settings
     */
    public function getPaymentSettings()
    {
        return [
            'etapestry' => $this->settings[$this->settings['env']]
        ];
    }

    /**
     * Utility method to determine if a NuSoap fault or error occurred.
     * If so, output any relevant info and stop the code from executing.
     */
    private function _checkStatus($nsc)
    {
        if ($nsc->fault || $nsc->getError()) {
            if (!$nsc->fault) {
                $exceptionMessage = 'Etapestry nsc error:' . $nsc->error;
                return new Failure($exceptionMessage);
            } else {
                $exceptionMessage = 'Etapestry nsc fault:' . $nsc->faultcode . ' :: ' . $nsc->faultstring;
                return new Failure($exceptionMessage);
            }
        }

        return true;
    }

    /**
     * Start an eTapestry API session by instantiating a
     * nusoap_client instance and calling the apiKeyLogin method.
     */
    private function _startEtapestrySession()
    {
        // Set login details. This info is visible to admin users within eTapestry.
        // Navigate to Management -> My Organization -> Subscriptions and look under
        // the API Subscription section.
        $databaseId = $this->_config['databaseId'];
        $apiKey     = $this->_config['apiKey'];

        // Set initial endpoint
        $endpoint = "https://sna.etapestry.com/v3messaging/service?WSDL";

        // Instantiate nusoap_client
        MachinePack::log('Establishing NuSoap Client...', 'debug');
        $nsc = new nusoap_client($endpoint, true);

        // Did an error occur?
        $status = $this->_checkStatus($nsc);
        if ($status instanceof Failure) {
            return $status;
        }

        // Invoke login method
        MachinePack::log('Calling etap login...', 'debug');
        $newEndpoint = $nsc->call("apiKeyLogin", array($databaseId, $apiKey));

        // Did a soap fault occur?
        $status = $this->_checkStatus($nsc);
        if ($status instanceof Failure) {
            return $status;
        }

        // Determine if the login method returned a value...this will occur
        // when the database you are trying to access is located at a different
        // environment that can only be accessed using the provided endpoint
        if ($newEndpoint != "") {
            MachinePack::log('New Endpoint:'. $newEndpoint, 'debug');

            // Instantiate nusoap_client with different endpoint
            MachinePack::log('Establishing NuSoap Client with new endpoint...', 'debug');
            $nsc = new nusoap_client($newEndpoint, true);

            // Did an error occur?
            $status = $this->_checkStatus($nsc);
            if ($status instanceof Failure) {
                return $status;
            }

            // Invoke login method
            MachinePack::log('Calling login method...', 'debug');
            $nsc->call("apiKeyLogin", array($databaseId, $apiKey));

            // Did a soap fault occur?
            $status = $this->_checkStatus($nsc);
            if ($status instanceof Failure) {
                return $status;
            }
        }

        // Output results
        MachinePack::log('Login Successful', 'debug');

        return $nsc;
    }

    /**
     * Start an eTapestry API session by calling the logout
     * method given a nusoap_client instance.
     */
    private function _stopEtapestrySession($nsc)
    {
        // Invoke logout method
        MachinePack::log('Calling logout method', 'debug');
        $nsc->call("logout");
        MachinePack::log('Done', 'debug');
    }

    /**
     * Get the first installment date
     * @param $event
     * @return \DateTime
     * @throws \Exception
     */
    private function _getFirstInstallmentDate($event)
    {
        if (isset($event['Intangible/Timezone.local'])
            && !empty($event['Intangible/Timezone.local'])
            && isset($event['Intangible/Timezone.etapestry'])
            && !empty($event['Intangible/Timezone.etapestry'])
        ) {
            $timeZoneLocal = new \DateTimeZone($event['Intangible/Timezone.local']);

            if (isset($event['Intangible/Subscription.firstInstallmentDate'])
                && !empty($event['Intangible/Subscription.firstInstallmentDate'])
            ) {
                $localDateTime = \DateTime::createFromFormat(
                    'd/m/Y',
                    $event['Intangible/Subscription.firstInstallmentDate'],
                    $timeZoneLocal
                );

                if (!$localDateTime) {
                    throw new \Exception('Invalid value for Intangible/Subscription.firstInstallmentDate');
                }
            } else {
                $localDateTime = new \DateTime('now', $timeZoneLocal);
            }

            $timeZoneDiff = $this->_calcTimeZoneHoursDiff(
                $event['Intangible/Timezone.local'],
                $event['Intangible/Timezone.etapestry']
            );

            $timeZoneHoursCorrection = $this->_calcTimeZoneHoursCorrection($localDateTime->format('H'), $timeZoneDiff);

            if ($timeZoneHoursCorrection > 0) {
                return $localDateTime->modify('+'.$timeZoneHoursCorrection.' hours');
            } else {
                return $localDateTime;
            }
        } else {
            return new \DateTime('now');
        }
    }

    /**
     * Calculate the hours diff between Timezones
     * @param $zone1
     * @param $zone2
     * @return int
     * @throws \Exception
     */
    private function _calcTimeZoneHoursDiff($zone1, $zone2)
    {
        $tz1     = new \DateTimeZone($zone1);
        $dateTz1 = new \DateTime('now', $tz1);

        $tz2     = new \DateTimeZone($zone2);
        $dateTz2 = new \DateTime('now', $tz2);

        $tz1Time = new \DateTime($dateTz2->format('Y-m-d H:i:s'));
        $tz2Time = new \DateTime($dateTz1->format('Y-m-d H:i:s'));

        return $tz1Time->diff($tz2Time)->h;
    }

    /**
     * Calculate the hours correction to get the same date for both timezones
     * @param $currentHours
     * @param int $timezoneDiff
     * @return int
     */
    private function _calcTimeZoneHoursCorrection($currentHours, $timezoneDiff = 15)
    {
        $diff = $timezoneDiff - $currentHours;
        if ($diff <= 0) {
            return 0;
        }

        return $diff;
    }

    /**
     * Take a United States formatted date (mm/dd/yyyy) and
     * convert it into a date/time string that NuSoap requires.
     */
    private function _formatDateAsDateTimeString($dateStr)
    {
        if ($dateStr == null || $dateStr == "") {
            return "";
        }
        if (substr_count($dateStr, "/") != 2) {
            return "[Invalid Date: $dateStr]";
        }

        $separator1 = stripos($dateStr, "/");
        $separator2 = stripos($dateStr, "/", $separator1 + 1);

        $month = substr($dateStr, 0, $separator1);
        $day   = substr($dateStr, $separator1 + 1, $separator2 - $separator1 - 1);
        $year  = substr($dateStr, $separator2 + 1);

        return ($month > 0 && $day > 0 && $year > 0) ?
            date(DATE_ATOM, mktime(date("H"), date("i"), date("s"), $month, $day, $year)) : "[Invalid Date: $dateStr]";
    }
}
