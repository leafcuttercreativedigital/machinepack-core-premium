<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Crmsync;

class Imis extends Handler
{
    private $_config;
    private $_access_token;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if ((empty($this->_config['username'])
            || empty($this->_config['password'])
            || empty($this->_config['apiUrl'])) && (empty($event['Intangible/Api.apiUrl'])
            || empty($event['Intangible/Api.username'])
            || empty($event['Intangible/Api.password']))
        ) {
            return new Failure(
                'Please add handler settings for ThankQ. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                apiUrl: "...",
                                username: "...",
                                password: "...",
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        //Generate Access token for request
        $this->_access_token = $this->_imisAuthToken($event);
        return $this->_submitRecord($event);
    }
    
    private function _submitRecord($event)
    {
        try {
            $contactID = $this->_checkContactExists($event);
            if ($contactID === null) {
                $contactID = $this->_createdContact($event);
            }

            if ($contactID) {
                $this->_createDonation($event, $contactID);
                return new Success(
                    [
                        'Intangible/Contact.identifier'     => $contactID
                    ]
                );
            }
            return new Failure('Unable to create contact');
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    private function _checkContactExists($event)
    {
        $url =  ($this->_config['apiUrl'] ?? $event['Intangible/Api.apiUrl']) . '/api/party?firstname=eq:'
        . $event['Person.givenName'] . '&lastname=eq:'
        . $event['Person.familyName'] . '&email=eq:' . $event['Person.email'];
        
        $header = array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->_access_token
        );

        $response = $this->_curl($url, $header, array('method' => 'GET'));
        return $response->Items->{'$values'}[0]->PartyId ?? null;
    }

    private function _createdContact($event)
    {
        $url    =  ($this->_config['apiUrl'] ?? $event['Intangible/Api.apiUrl']) . '/api/party';
        $header = array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->_access_token
        );
        $body   = array (
            "method" => "POST",
            '$type' => "Asi.Soa.Membership.DataContracts.PersonData, Asi.Contracts",
            "PersonName" =>  array (
                '$type' => "Asi.Soa.Membership.DataContracts.PersonNameData, Asi.Contracts",
                "FirstName" => $event['Person.givenName'],
                "InformalName" => $event['Person.givenName'],
                "LastName" => $event['Person.familyName'],
                "FullName" => $event['Person.givenName'] . ' ' . $event['Person.familyName'],
            ),
            "Addresses" => array (
                '$type' => "Asi.Soa.Membership.DataContracts.FullAddressDataCollection, Asi.Contracts",
                '$values' => array (
                    array (
                        '$type' => "Asi.Soa.Membership.DataContracts.FullAddressData, Asi.Contracts",
                        "Address" => array (
                            '$type' => "Asi.Soa.Membership.DataContracts.AddressData, Asi.Contracts",
                            "PostalCode" => $event['PostalAddress.postalCode'],
                        ),
                        "AddressPurpose" => "Address",
                        "Phone" => $event['Person.telephone'],
                        "DisplayName" => $event['Person.givenName'],
                        "Email" => $event['Person.email'],
                    ),
                ),
            ),
            "Emails" => array(
                '$type' => "Asi.Soa.Membership.DataContracts.EmailDataCollection, Asi.Contracts",
                '$values' => array(
                    array(
                        '$type' => "Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts",
                        "Address" => $event['Person.email'],
                        "EmailType" => "_Primary",
                        "IsPrimary" => true
                    ),
                    array(
                        '$type' => "Asi.Soa.Membership.DataContracts.EmailData, Asi.Contracts",
                        "Address" => $event['Person.email'],
                        "EmailType" => "Address"
                   )
                )
            ),
            "Phones" => array (
                '$type' => "Asi.Soa.Membership.DataContracts.PhoneDataCollection, Asi.Contracts",
                '$values' => array (
                    array (
                        '$type' => "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                        "Number" => $event['Person.telephone'],
                        "PhoneType" => "Mobile",
                    ),
                    array (
                        '$type' => "Asi.Soa.Membership.DataContracts.PhoneData, Asi.Contracts",
                        "Number" =>  $event['Person.telephone'],
                        "PhoneType" => "Address",
                    ),
                ),
            )
        );

        $response = $this->_curl($url, $header, $body);
        return $response->PartyId ?? null;
    }

    private function _createDonation($event, $contactID)
    {
        $url    =  ($this->_config['apiUrl'] ?? $event['Intangible/Api.apiUrl']) . '/api/ComboOrder';
        $header = array(
            "Content-Type: application/json",
            "Authorization: Bearer " . $this->_access_token
        );
        /**
         * phpcs:disable
         */
        $body     = array(
            '$type' => 'Asi.Soa.Commerce.DataContracts.ComboOrderData, Asi.Contracts',
            'Currency' => [
                '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                'DecimalPositions' => 2,
            ],
            'Order' => [
                '$type' => 'Asi.Soa.Commerce.DataContracts.OrderData, Asi.Contracts',
                'BillToCustomerParty' => [
                    '$type' => 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
                    'PartyId' => $contactID,
                ],
                'Currency' => [
                    '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                    'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                    'DecimalPositions' => 2,
                ],
                'Lines' => [
                '$type' => 'Asi.Soa.Commerce.DataContracts.OrderLineDataCollection, Asi.Contracts',
                '$values' => [
                        [
                            '$type' => 'Asi.Soa.Commerce.DataContracts.OrderLineData, Asi.Contracts',
                            'OrderLineId' => $event['Order.identifier'],
                            'ExtendedAmount' => [
                                '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                                'Amount' => $event['MonetaryAmount.value'],
                                'Currency' => [
                                '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                                'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                                'DecimalPositions' => 2,
                                ],
                                'IsAmountDefined' => true,
                            ],
                            'Item' => [
                                '$type' => 'Asi.Soa.Commerce.DataContracts.ItemSetItemData, Asi.Contracts',
                                'ItemSetType' => 1,
                                'ItemCode' => 'GENERALDONATION',
                                'Name' => 'General Donation',
                            ],
                            'LineNumber' => 1,
                            'QuantityBackordered' => [
                                '$type' => 'System.Nullable`1[[Asi.Soa.Commerce.DataContracts.QuantityData, Asi.Contracts]], mscorlib',
                            ],
                            'UnitPrice' => [
                                '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                                'Amount' => $event['MonetaryAmount.value'],
                                'Currency' => [
                                    '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                                    'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                                    'DecimalPositions' => 2,
                                ],
                                'IsAmountDefined' => true,
                            ],
                            'BaseUnitPrice' => [
                                '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                                'Amount' => $event['MonetaryAmount.value'],
                                'Currency' => [
                                    '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                                    'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                                    'DecimalPositions' => 2,
                                ],
                                'IsAmountDefined' => true,
                            ],
                            'Discount' => [
                                '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                                'Currency' => [
                                    '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                                    'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                                    'DecimalPositions' => 2,
                                ],
                                'IsAmountDefined' => true,
                            ],
                            'SourceCode' => '',
                        ],
                    ],
                ],
                'LineTotal' => [
                    '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Amount' => $event['MonetaryAmount.value'],
                    'Currency' => [
                        '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                        'DecimalPositions' => 2,
                    ],
                    'IsAmountDefined' => true,
                ],
                'MiscellaneousChargesTotal' => [
                    '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Currency' => [
                        '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                        'DecimalPositions' => 2,
                    ],
                    'IsAmountDefined' => true,
                ],
                'OrderDate' => $event['Order.orderDate'] ?? date('Y-m-d\TH:i:s\Z'),
                'OrderTotal' => [
                    '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Amount' => $event['MonetaryAmount.value'],
                    'Currency' => [
                        '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                        'DecimalPositions' => 2,
                    ],
                    'IsAmountDefined' => true,
                ],
                'OriginatorCustomerParty' => [
                    '$type' => 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
                    'PartyId' => $contactID,
                ],
                'ShippingTotal' => [
                    '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Currency' => [
                        '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                        'DecimalPositions' => 2,
                    ],
                    'IsAmountDefined' => true,
                ],
                'SoldToCustomerParty' => [
                    '$type' => 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
                    'PartyId' => $contactID,
                ],
                'TotalBasePrice' => [
                    '$type' => 'System.Nullable`1[[Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts]], mscorlib',
                    'Amount' => $event['MonetaryAmount.value'],
                    'Currency' => [
                        '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                        'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                        'DecimalPositions' => 2,
                    ],
                    'IsAmountDefined' => true,
                ],
            ],
            'Payments' => [
                '$type' => 'Asi.Soa.Commerce.DataContracts.RemittanceDataCollection, Asi.Contracts',
                '$values' => [
                    [
                        '$type' => 'Asi.Soa.Commerce.DataContracts.RemittanceData, Asi.Contracts',
                        'Amount' => [
                            '$type' => 'Asi.Soa.Core.DataContracts.MonetaryAmountData, Asi.Contracts',
                            'Amount' => $event['MonetaryAmount.value'],
                            'Currency' => [
                                '$type' => 'Asi.Soa.Core.DataContracts.CurrencyData, Asi.Contracts',
                                'CurrencyCode' => $event['MonetaryAmount.currency'] ?? "AUD",
                                'DecimalPositions' => 2,
                            ],
                            'IsAmountDefined' => true,
                        ],
                        'PaymentMethod' => [
                            '$type' => 'Asi.Soa.Commerce.DataContracts.PaymentMethodData, Asi.Contracts',
                            'PaymentMethodId' => 'CASH',
                            'PaymentType' => 'CASH',
                            'GatewayAccountId' => '',
                        ],
                        'PayorParty' => [
                            '$type' => 'Asi.Soa.Commerce.DataContracts.CustomerPartyData, Asi.Contracts',
                            'PartyId' => $contactID,
                        ],
                        'ReferenceNumber' => $event['Order.identifier'],
                    ],
                ],
            ]
        );
        /** phpcs:enable */
        $response = $this->_curl($url, $header, $body);
    }

    /**
     * Handle auth token
     * @param  Crmsync $event    user information
     * @return auth token
     */
    private function _imisAuthToken($event)
    {
        $url       =  ($this->_config['apiUrl'] ?? $event['Intangible/Api.apiUrl']) . '/token';
        $headers   = array(
            "Content-Type: application/x-www-form-urlencoded"
        );
        $auth_data = [
            'method'        => 'POST',
            'grant_type'    => 'password',
            'username'      =>  $this->_config['username'] ?? $event['Intangible/Api.username'],
            'password'      =>  $this->_config['password'] ?? $event['Intangible/Api.password']
        ];

        $response = $this->_curl($url, $headers, $auth_data, true);
        return $response->access_token;
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    private function _curl($url, $header, $data = array(), $url_params = false)
    {
        try {
            $method = $data['method'] ?? 'POST';
            unset($data['method']);
            if ($url_params) {
                $input_data = http_build_query($data);
            } else {
                $input_data = json_encode($data);
            }
            
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);
           
            return json_decode($result);
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }
}
