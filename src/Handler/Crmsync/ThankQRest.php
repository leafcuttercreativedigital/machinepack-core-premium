<?php
namespace MachinePack\Core\Handler\Crmsync;

use MachinePack\Core\MachinePack;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Ignored;
use MachinePack\Core\Result\Success;
use MachinePack\Core\Result\Failure;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Event\Events\Crmsync;

class ThankQRest extends Handler
{
    const DEFAULT_CONTACT_SOURCE   = 'DONONLINE';
    const DEFAULT_SOURCE_CODE      = 'DONONLINE';
    const DEFAULT_SOURCE_CODE_2    = 'DONONLINE';
    const DEFAULT_DESTINATION_CODE = 'DONONLINE';

    private $_config;
    private $_contactSource;
    private $_sourceCode;
    private $_sourceCode2;
    private $_destinationCode;

    public function handleEvent(\MachinePack\Core\Event\Event $event): Result
    {
        if (! $event instanceof Crmsync) {
            return new Ignored;
        }

        //load config from settings
        $this->_config = $this->settings[$this->settings['env']];

        //validate configuration variables
        if ((empty($this->_config['username'])
            || empty($this->_config['password'])
            || empty($this->_config['apiUrl'])            ) && (empty($event['Intangible/ThankQ_Rest.apiUrl'])
            || empty($event['Intangible/ThankQ_Rest.username'])
            || empty($event['Intangible/ThankQ_Rest.password']))
        ) {
            return new Failure(
                'Please add handler settings for ThankQ. Full config should be:
                    {
                        config: {
                            env: <someenv>,
                            <someenv>: {
                                apiUrl: "...",
                                username: "...",
                                password: "...",
                                isProductionMode: "..."
                            }
                        }
                    }
                '
            );
        }

        //set contact source
        if (isset($event['Intangible/ThankQ.contactSourceCode'])) {
            $this->_contactSource = $event['Intangible/ThankQ.contactSourceCode'];
        } else {
            $this->_contactSource = self::DEFAULT_CONTACT_SOURCE;
        }

        //set source code
        if (isset($event['Intangible/ThankQ.sourceCode'])) {
            $this->_sourceCode = $event['Intangible/ThankQ.sourceCode'];
        } else {
            $this->_sourceCode = self::DEFAULT_SOURCE_CODE;
        }

        //set source code 2
        if (isset($event['Intangible/ThankQ.sourceCode2'])) {
            $this->_sourceCode2 = $event['Intangible/ThankQ.sourceCode2'];
        } else {
            $this->_sourceCode2 = self::DEFAULT_SOURCE_CODE_2;
        }

        //set destination code
        if (isset($event['Intangible/ThankQ.destinationCode'])) {
            $this->_destinationCode = $event['Intangible/ThankQ.destinationCode'];
        } else {
            $this->_destinationCode = self::DEFAULT_DESTINATION_CODE;
        }

        //depending on one-off or recurring, make payment
        if ($event['Intagible/Event.action'] === 'recurring donation') {
            return $this->_processSubscription($event);
        }

        if ($event['Intagible/Event.action'] === 'user creation') {
            return $this->_processUser($event);
        }

        return $this->_processPayment($event);
    }

    /**
     * Handle a user creation request
     * @param  Crmsync $event    user information
     * @return Success|Failure
     */
    private function _processUser($event)
    {
        try {
            $contactID = $this->_createContact($event);
            if ($contactID) {
                $profileID = $this->_createProfile($event, $contactID);

                if ($profileID) {
                    return new Success(
                        [
                            'profileID' => $profileID,
                        ]
                    );
                }

                return new Failure(
                    'The profile ID was not received.'
                );
            }

            return new Failure(
                'The contact ID was not received.'
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Handle a single payment request
     * @param  Crmsync $event    payment information
     * @return Success|Failure
     */
    private function _processPayment($event)
    {
        try {
            $contactID = $this->_createContact($event);
            if ($contactID) {
                if (isset($event['Intagible/Profile.type']) && $event['Intagible/Profile.type']) {
                    $tributeProfileID = $this->_createInMemoryProfile($event, $contactID);
                }
                if (isset($event['Intagible/Event.utm_source']) && $event['Intagible/Event.utm_source']) {
                    $utmProfileID = $this->_createUTMProfile($event, $contactID);
                }
                $paymentID = $this->_createDonation($event, $contactID);

                if ($paymentID) {
                    return new Success(
                        [
                            'Intangible/Contact.identifier'     => $contactID,
                            'Intangible/Record.identifier'      => $paymentID,
                            'Intangible/Tribute.identifier'     => $utmProfileID,
                            'Intangible/UTM.identifier'         => $tributeProfileID,
                        ]
                    );
                }

                return new Failure(
                    'The payment ID was not received.'
                );
            }

            return new Failure(
                'The contact ID was not received.'
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
    }

    /**
     * Handle a recurring payment request
     * @param  Crmsync $event    payment information
     * @return Success|Failure
     */
    private function _processSubscription($event)
    {
        try {
            $contactID = $this->_createContact($event);
            if ($contactID) {
                if (isset($event['Intagible/Profile.type']) && $event['Intagible/Profile.type']) {
                    $tributeProfileID = $this->_createInMemoryProfile($event, $contactID);
                }
                if (isset($event['Intagible/Event.utm_source']) && $event['Intagible/Event.utm_source']) {
                    $utmProfileID = $this->_createUTMProfile($event, $contactID);
                }
                $pledgeID  = $this->_createPledge($event, $contactID);
                $paymentID = $this->_createDonation($event, $contactID, $pledgeID);

                if ($paymentID) {
                    return new Success(
                        [
                            'Intangible/Contact.identifier'     => $contactID,
                            'Intangible/Pledge.identifier'      => $pledgeID,
                            'Intangible/Record.identifier'      => $paymentID,
                            'Intangible/Tribute.identifier'     => $tributeProfileID,
                            'Intangible/UTM.identifier'         => $utmProfileID,

                        ]
                    );
                }

                return new Failure(
                    'The payment ID was not received.'
                );
            }

            return new Failure(
                'The contact ID was not received.'
            );
        } catch (\Exception $e) {
            MachinePack::log($e->getMessage(), 'error', $e->getTraceAsString());
            return new Failure(
                $e->getMessage()
            );
        }
       
        return new Success($responseData);
    }

    private function _createContact($event)
    {
        $api_url = $this->_config['apiUrl'] ?? $event['Intangible/ThankQ_Rest.apiUrl'];
        $url     = $api_url . '/Contacts';
        $contact = array(
            'method' => 'POST',
            'contacttype' =>  $event['Intangible.contactType'] ?? "",
            'title' => $event['Person.honorificPrefix'] ?? "",
            'firstname' => $event['Person.givenName'] ?? "",
            'keyname' => $event['Person.familyName'] ?? "",
            'careof' => $event['Organization.name'] ?? "",
            'addressline1' => $event['PostalAddress.streetAddress'] ?? "",
            'addressline2' => $event['PostalAddress.streetAddress2'] ?? "",
            'suburb' => $event['PostalAddress.addressLocality'] ?? "",
            'state' => $event['PostalAddress.addressRegion'] ?? "",
            'postcode' => $event['PostalAddress.postalCode'] ?? "",
            'country' => $event['PostalAddress.addressCountry'] ?? "",
            'daytelephone' =>  $event['Person.telephone'] ?? "",
            'emailaddress' =>  $event['Person.email'] ?? "",
            'dateofbirth' => $event['Person.birthDate'] ?? "",
            'source' => $this->_contactSource,
            'bequestprospect' => $event['Intangible/ThankQ.bequest'] ?? "",
        );

        $optional_fields = [
            'primarycategory' => 'Contact.primarycategory',
            'mobilenumber' => 'Person.mobilenumber'
        ];

        foreach ($optional_fields as $api_key => $event_key) {
            if (!empty($event[$event_key])) {
                $contact[$api_key] = $event[$event_key];
            }
        }

        $response = json_decode($this->_curl($url, $event, $contact));
        if ($response && isset($response->Id)) {
            $contactId = $response->Id;
            return $contactId;
        }
        return false;
    }

    private function _createInMemoryProfile($event, $contactID)
    {
        $api_url = $this->_config['apiUrl'] ?? $event['Intangible/ThankQ_Rest.apiUrl'];
        $url     = $api_url . '/Contacts/'.
                    $contactID . '/Profiles';

        $profile = array(
            'method' => 'POST',
            'serialnumber' => $contactID,
            'name' =>  $event['Intagible/Profile.type'],
            'value' => $event['MonetaryAmount.value'],
            'effectivefrom' => $event['Order.orderDate'],
            'note' => $event['Intagible/Profile.firstname'] . ' ' . $event['Intagible/Profile.lastname'],
            'additional1' => $event['Order.identifier'],
        );

        $response = $this->_curl($url, $event, $profile);
        return $response;
    }

    private function _createUTMProfile($event, $contactID)
    {
        $api_url = $this->_config['apiUrl'] ?? $event['Intangible/ThankQ_Rest.apiUrl'];
        $url     = $api_url . '/Contacts/'.
                    $contactID . '/Profiles';

        $utm_string = '';
        if (isset($event['Intagible/Event.utm_source']) && $event['Intagible/Event.utm_source']) {
            $utm_string .= 'utm_source=' . $event['Intagible/Event.utm_source'] . '&';
        }
        if (isset($event['Intagible/Event.utm_medium']) && $event['Intagible/Event.utm_medium']) {
            $utm_string .= 'utm_medium=' . $event['Intagible/Event.utm_medium'] . '&';
        }
        if (isset($event['Intagible/Event.utm_campaign']) && $event['Intagible/Event.utm_campaign']) {
            $utm_string .= 'utm_campaign=' . $event['Intagible/Event.utm_campaign'] . '&';
        }
        if (isset($event['Intagible/Event.utm_content']) && $event['Intagible/Event.utm_content']) {
            $utm_string .= 'utm_content=' . $event['Intagible/Event.utm_content'] . '&';
        }
        if (isset($event['Intagible/Event.utm_term']) && $event['Intagible/Event.utm_term']) {
            $utm_string .= 'utm_term=' . $event['Intagible/Event.utm_term'];
        }
        $utm_string = rtrim($utm_string, "&");

        $profile = array(
            'method' => 'POST',
            'serialnumber' => $contactID,
            'name' =>  'UTM Tracking Code',
            'value' => $utm_string,
            'effectivefrom' => $event['Order.orderDate'],
            'additional1' => $event['Order.identifier'],
        );

        $response = $this->_curl($url, $event, $profile);
        return $response;
    }

    private function _createProfile($event, $contactID)
    {
        $api_url = $this->_config['apiUrl'] ?? $event['Intangible/ThankQ_Rest.apiUrl'];
        $url     = $api_url . '/Contacts/'.
                    $contactID . '/Profiles';

        $profile = array(
            'method' => 'POST',
            'name' =>  'Subscribe to',
            'value' => $event['Person.givenName'] . ' ' . $event['Person.familyName']
        );

        $response = $this->_curl($url, $event, $profile);
        return $response;
    }

    private function _createDonation($event, $contactID, $pledgeID = null)
    {
        $api_url = $this->_config['apiUrl'] ?? $event['Intangible/ThankQ_Rest.apiUrl'];
        $url     = $api_url . '/Contacts/' . $contactID . '/Donations';

        $paymentCreditCard = $this->_createCreditCard($event);

        $donation = array(
            'method' => 'POST',
            'Serialnumber' => $contactID,
            'dateofpayment' => $event['Order.orderDate'] ?? "",
            'paymenttype' => $event['Order.paymentMethod'] ?? "",
            'paymentamount' => $event['MonetaryAmount.value'] ?? "",
            'paymentamountnett' => $event['MonetaryAmount.value'] ?? "",
            'receiptrequired' => 'No',
            'receiptsummary' => 'No',
            'sourcecode' => $this->_sourceCode,
            'destinationcode' => $this->_destinationCode,
            'campaignyear' => $event['Intangible/ThankQ.campaignYear'] ?? "",
            'creditcard' => $paymentCreditCard,
            'webstatus' => $event['Order.orderStatus'] ?? "",
            'notes'     => $event['Intangible/ThankQ.notes'] ?? ""
        );

        $optional_fields = [
            'sourcecode2' => 'Intangible/ThankQ.sourceCode2',
            'manualreceiptno' => 'Order.receiptNo',
            'webauthorisation' => 'Order.paymentId'
        ];

        foreach ($optional_fields as $api_key => $event_key) {
            if (!empty($event[$event_key])) {
                $contact[$api_key] = $event[$event_key];
            }
        }

        if ($pledgeID) {
            $donation['externalref']                  = $pledgeID;
            $donation['externalreftype']              = 'Pledge';
            $donation['thispaymentisfirstinstalment'] = $event['Intangible/ThankQ.paymentisfirst'] ?? "";
        }

        $response = $this->_curl($url, $event, $donation);
        return $response;
    }

    private function _createPledge($event, $contactID)
    {
        $api_url = $this->_config['apiUrl'] ?? $event['Intangible/ThankQ_Rest.apiUrl'];
        $url     = $api_url . '/Contacts/' .
                                        $contactID . '/Pledges';

        $paymentCreditCard = $this->_createCreditCard($event);

        $pledge = array(
            'method' => 'POST',
            'Serialnumber' => $contactID,
            'pledgestatus' => 'Active',
            'description' => $event['Intangible/Pledge.description'] ?? 'Monthly pledge',
            'pledgeplan' => 'Continuous',
            'pledgetype' => 'Regular Payment',
            'currency' => $event['MonetaryAmount.currency'] ?? "",
            'campaignyear' => $event['Intangible/ThankQ.campaignYear'] ?? "",
            'paymenttype' => $event['Order.paymentMethod'] ?? "",
            'receiptrequired' => 'No',
            'receiptsummary' => 'Yes',
            'receiptfrequency' => 'Monthly',
            'nextreceiptdate' => $event['Intangible/ThankQ.nextinstalment'] ?? "",
            'startdate' => $event['Intangible/ThankQ.startDate'] ?? date('Y-m-d H:i:s A', strtotime('+1 month')),
            'enddate' => '',
            'totalvalue' => $event['MonetaryAmount.value'] ?? "",
            'instalmentvalue' => $event['MonetaryAmount.value'] ?? "",
            'maxpayments' => 100,
            'creditcard' => $paymentCreditCard,
            'NextInstalmentDate' => $event['Intangible/ThankQ.nextinstalment'] ?? "",
            'NextInstalmentAmount' => $event['MonetaryAmount.value'] ?? "",
            'taxclaimable' => 'Yes',
            'oneoff' => 'No',
            'paymentfrequency' => $event['Intangible/ThankQ.paymentfrequency'] ?? "Four-Weekly",
            'ownercode' => 0,
            'pledgedestinations' => [
                array(
                    'sourcecode' => $this->_sourceCode,
                    'sourcecode2' => $this->_sourceCode2,
                    'destinationcode' => $this->_destinationCode,
                )
            ]
        );

        $response = $this->_curl($url, $event, $pledge);
        return $response;
    }

    private function _createCreditCard($event)
    {
        $exp_month = $event['CreditCard/CardDetails.expiryMonth'] ?? "";
        $exp_year  = $event['CreditCard/CardDetails.expiryYear'] ?? "";
        $expiry    = "";
        
        if ($exp_month && $exp_year) {
            $expiry = strval($exp_month) . '/' . strval($exp_year);
        }
        return array(
            'creditcardtype'    => $event['CreditCard/CardDetails.type'] ?? "",
            'creditcardholder'  => $event['CreditCard/CardDetails.name'] ?? "",
            'creditcardmasked'  => $this->_ccMasking($event['CreditCard/CardDetails.number']),
            'creditcardnumber'  => $event['CreditCard/CardDetails.number'] ?? "",
            'creditcardexpiry'  => $expiry,
            'creditCardToken'   => $event['CreditCard/CardDetails.token'] ?? ""
        );
    }

    /**
     * Make a curl request
     *
     * @param string $method    GET|POST
     * @param string $url       the url to call
     * @param string $bearer    the bearer token for authentication
     * @param array $data       request payload
     * @return void
     */
    private function _curl($url, $event, $data = array())
    {
        try {
            $method = $data['method'] ?? 'POST';
            unset($data['method']);
            $input_data = json_encode($data);
            $headers    = array(
                "Content-Type: application/json"
            );

            $curl = curl_init();

            $username = $this->_config['username'] ?? $event['Intangible/ThankQ_Rest.username'];
            $password = $this->_config['password'] ?? $event['Intangible/ThankQ_Rest.password'];

            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_USERPWD, $username . ":" . $password);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PATCH') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, $input_data);
            } elseif ($method == 'PUT') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
            } elseif ($method == 'DELETE') {
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'DELETE');
            } else {
                curl_setopt($curl, CURLOPT_HEADER, 0);
            }

            $result = curl_exec($curl);
            curl_close($curl);
           
            return $result;
        } catch (Exception $ex) {
            $exceptionMsg = $ex->getMessage();
            $returnArray  = array('Errors' => true, 'message' => $exceptionMsg);
            return json_encode($returnArray);
        }
    }

    private function _ccMasking($number = '4111111111111111', $maskingCharacter = '*')
    {
        if (!$number) {
            return;
        }
        return substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4);
    }
}
