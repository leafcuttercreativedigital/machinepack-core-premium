<?php
namespace MachinePack\Core\Handler\Logger;

use MachinePack\Core\Event\Event;
use MachinePack\Core\Handler\Handler;
use MachinePack\Core\Result\Result;
use MachinePack\Core\Exception\Semantic as SemanticException;

class InsightsAI extends Handler
{
    public function handleEvent(Event $event): Result
    {
        throw new SemanticException('Nothing working here yet');
    }
}
