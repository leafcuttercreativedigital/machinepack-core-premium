# MachinePack Premium

This package adds premium integrations / handlers to the MachinePack project.

This package should be set to private.

## Formatting and testing all handlers

Use the command 

bin/watch.sh 

to run PHP CS, PHP CBF and PHP Unit tests. Note this command uses entr, you will need to install that first.

## Testing single handlers

You can use the below command to test just one handler - useful if you dont want to wait for every other handler to come through correctly

find src/ test/ -name '*.php' | vendor/bin/phpunit --filter 'MachinePack\\Core\\Test\\Unit\\MachinePackSecureCoTest'
